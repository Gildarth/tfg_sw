package ExampleCorrect;

/*
 *	@pattern State
 *	@patternElement State orderdep
 */
public interface State {

	public int abrir(Context context);

	public int mensaje (Context context, String string, int number);

	public int cerrar(Context context);
}