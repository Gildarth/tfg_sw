package ExampleCorrect;

/*
 *	@pattern State
 *	@patternElement ConcreteState
 */
public class Abierta implements State {

	public int abrir(Context context) {
		return 500;
	}

	/*
	 * @patternAction Transition Abierta <context, "test", 200>
	 * @patternAction Transition Cerrada <context, "test", 500>
	 */
	public int mensaje(Context context, String string, int number) {
		if (number == 500)
			context.setState(new Cerrada());
		return 200;
	}

	/*
	 * @patternAction Transition Cerrada <context>
	 */
	public int cerrar(Context context) {
		context.setState(new Cerrada());
		return 200;
	}
}