package ExampleCorrect;


/*
 *	@pattern State <State>
 *	@patternElement Context
 *	@patternElement Builder <new Context (new Cerrada())>
 */
public class Context {
	
	private State state;
	
	public Context (State initialState) {
		this.state = initialState;
	}
	
	public State getState () {
		return state;
	}
	
	public void setState (State state) {
		this.state = state;
	}
	
}