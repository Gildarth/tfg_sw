package ExampleCorrect;

/*
 *	@pattern State
 *	@patternElement ConcreteState initial final
 */
public class Cerrada implements State {

	/*
	 * @patternAction Transition Abierta
	 */
	public int abrir(Context context) {
		context.setState(new Abierta());
		return 200;
	}

	public int mensaje(Context context, String string, int number) {
		return 500;
	}

	public int cerrar(Context context) {
		return 500;
	}
}
