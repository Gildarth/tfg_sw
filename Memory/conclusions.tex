\chapter{Conclusiones}
\minitoc
\label{chap:conclusiones}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Objetivo: Contar cómo está ahora el proyecto, si ha merecido la              %
%           pena, lo que se ha aprendido, si se aplicaría de nuevo, etc.       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

En este apartado se presenta el cumplimiento de objetivos iniciales, junto con las conclusiones extraídas y las lecciones aprendidas de los distintos problemas surgidos durante el desarrollo del proyecto.

\section{Consecución de objetivos}

En general, se considera que el resultado del proyecto es satisfactorio, ya que ha dado lugar a un producto que permite generar pruebas relevantes y necesarias de una manera sencilla y efectiva con un mínimo esfuerzo por parte del usuario, ya que para las pruebas básicas de los patrones soportados solo ha de poner las anotaciones en su código y para las pruebas más avanzadas la intervención adicional requerida es mínima (decidir una secuencia de acciones en PFE-2) o inevitable (definir una forma de comparar los resultados de las estrategias en PFEST-1).

Por ello se cree que el uso del producto conseguido ayudará a fomentar el uso consciente e intencionado de patrones al añadir a los beneficios tradicionales sobre proceso de diseño, los beneficios adicionales de la obtención automática de implementación de pruebas unitarias significativas.

Más en concreto, al final del plazo del proyecto se han cumplido la mayor parte de los objetivos marcados, incluyendo los más fundamentales, consistentes en el desarrollo del sistema base, la prueba de su facilidad de ampliación para soportar nuevos patrones y la capacidad de detección de errores de las pruebas diseñadas. No obstante, por problemas con la planificación (esfuerzo subestimado), no se ha conseguido completar la última iteración, consistente en extender la funcionalidad para dar soporte al patrón Observador.
	
	Dando una visión detallada objetivo por objetivo, podemos decir:

	\begin{description}
		
		\item [Objetivo-1:] \textit{Que el sistema sea escalable, de forma que añadir el soporte a nuevos patrones suponga el menor esfuerzo posible.}
		
			Se considera cumplido este objetivo al haberse implementado soporte para un segundo patrón, comprobando que dicha extensión no implicó modificaciones significativas en el sistema base ya implementado. 
			
			Una prueba empírica de esta afirmación se obtiene al comparar el esfuerzo de desarrollo de la primera iteración con la segunda, en la cual, gracias a estar basada en la primera, se necesito un esfuerzo muchísimo menor.
		
		\item [Objetivo-2:] \textit{Analizar el patrón Estado y diseñar un conjunto de pruebas genéricas, independientes y de generación lo más automatizada posible.}
		
			Este objetivo se considera cumplido al ejecutarse las pruebas de aceptación sobre los ejemplos preparados, de manera que éstas detectan los errores introducidos intencionalmente a este propósito y no arrojan falsos positivos.
		
		\item [Objetivo-3:]\textit{ Implementar el soporte del sistema para el patrón Estado.}
		
			Se da por satisfecho este objetivo con el código implementado que da soporte al patrón Estado, en el cual no se han conseguido detectar fallos con las pruebas unitarias realizadas pese al esfuerzo dedicado en ellas.
		
		\item [Objetivo-4:] \textit{Analizar el patrón Estrategia y diseñar un conjunto de pruebas genéricas, independientes y de generación lo más automatizada posible.}
		
			Al igual que en el \textbf{Objetivo-2}, este objetivo se considera cumplido con las pruebas de aceptación.
		
		\item [Objetivo-5:] \textit{Implementar el soporte del sistema para el patrón Estrategia.}
		
			De la misma manera que con el \textit{Objetivo-3}, al no ser capaces de detectar fallos con las pruebas unitarias realizadas, se considera cumplido el objetivo.
		
		\item [Objetivo-6:] \textit{Analizar el patrón Observador y diseñar un conjunto de pruebas genéricas, independientes y de generación lo más automatizada posible.}
		
			Por falta de tiempo y debido a los desvíos sufridos durante la primera iteración, este objetivo no ha podido cumplirse.
		
		\item [Objetivo-7:] \textit{Implementar el soporte del sistema para el patrón Observador.}
		
			Sin el análisis pertinente, la implementación del soporte para el patrón Observador no ha sido llevada a cabo.
		
	\end{description}
	
	En resumen, se han alcanzado todos los objetivos a excepción de los referentes al patrón Observador (tercera iteración). Aun así, puesto que el objetivo principal de obtener un sistema robusto y escalable ha sido alcanzado, se ha conseguido implementar el soporte para dos de tres patrones y el esfuerzo total ha sido el esperado para el proyecto completo, se considera que el error está en la estimación y planificación de la primera iteración, habiendo sido demasiado optimista.

\section{Lecciones aprendidas}

	Una vez diseñado el sistema de representación intermedia por XML, al volver la vista atrás se ha llegado a la conclusión de que no fue la mejor decisión existiendo la alternativa de JSON, pues al final la estructura interna de representación de los patrones en los dos módulos ha resultado ser prácticamente idéntica, por lo que una librería de JSON podría haber serializado y deserializado hacia y desde JSON directamente. Cuando se llegó a esta conclusión, la primera iteración ya estaba terminada y el coste de modificar la primera iteración y hacer la segunda ya con JSON (unas 30 horas $\times$ persona) habría sido más grande que continuar con el sistema actual e implementar la segunda iteración con XML usando la estructura de la primera como guía (unas 10 horas $\times$ persona), pues si bien no habría que diseñar el lenguaje XML, sí habría que documentar y especificar minuciosamente las clases a serializar, obligando a terceros que quisieran ampliar el sistema a adaptarse a dicha estructura de clases y a hacer otra con la que trabajar si quieren añadirle campos diferentes a los que se han de serializar o que te ofrece la deserialización. Por lo tanto, ya que la desviación en tiempo y esfuerzo ya era importante y no suponía realmente un problema a la hora de continuar el proyecto, se continuó con el sistema existente.
	
	Un error grave fue el cometido a la hora de estimar el esfuerzo y tiempo totales del proyecto durante la planificación inicial, habiendo dado lugar a una mala estimación, como se ha hecho patente al no haberse podido desarrollar la tercera iteración por el desvío temporal, y aun y así haber llegado a la cantidad de esfuerzo estimada inicialmente para el proyecto completo, con una desviación acumulada en las dos iteración implementadas de 22 horas $\times$ persona. Incluso aunque no hubiera habido el desvío temporal, en base al esfuerzo realizado en la segunda iteración, de haber desarrollado la tercera iteración habría conllevado otras 60 horas $\times$ persona por lo menos, suponiendo un desvío en esfuerzo de 63 horas $\times$ persona, lo que supone un 20\% de la planificación inicial.
	
\section{Trabajo futuro}

	Como trabajo futuro se propone, en primera instancia, la ampliación del sistema para soportar más patrones de un lenguaje ya soportado usando lo ya implementado como guía. Se estima, fijándose en el coste de esfuerzo de la segunda iteración (69 horas $\times$ persona), que ampliar para un nuevo patrón pueda llevar entre 50 y 90 horas $\times$ persona en función de la familiaridad del desarrollador con el proyecto y la dificultad técnica de análisis del patrón y diseño de sus pruebas.
	
	En segunda instancia se propone la ampliación para soportar diferentes lenguajes. Esto repercutiría en la necesidad de ampliar la parte común de forma que sepa procesar diferentes lenguajes, para los cuales los marcadores pueden o no identificarse de la misma forma (\@ dentro de comentarios de bloque actualmente), pero no supondría la necesidad, una vez procesados los marcadores a la representación interna, de modificar la forma de trabajo de los parsers concretos, siendo posible incluso la reutilización completa o casi completa con poco esfuerzo pues no trabajan con el fichero de código sino con los marcadores ya procesados, la información de los cuales debiera ser igual o muy similar (por ejemplo, puede variar cómo se pasan los parámetros a un método, pero al trabar con patrones para la orientación a objetos, no es habitual). Por ello, en función del grado de reutilización posible y de la familiaridad del desarrollador con el proyecto, se estima que añadir un nuevo lenguaje al sistema para un primer patrón costaría un esfuerzo de entre 40 y 120 horas $\times$ persona, correspondientes respectivamente a las combinaciones de ``alta reutilización y familiaridad'' y de ``nula reutilizacion y baja familiaridad''.
	
	Como tercera mejora, se podría extender el sistema dando soporte a la actualización del código de pruebas generado en lugar de ser completamente recreado cada vez que se produzcan cambios en la implementación, con lo que en lugar de regenerar completamente, se analizarían los cambios necesarios poder para mantener los posibles cambios manuales que el usuario hubiera podido realizar en dichas pruebas. Esto solucionaría situaciones como por ejemplo, cuando el usuario ha completado la prueba de PFE-2 y vuelve a ejecutar el sistema sobre las mismas clases con la misma ruta de salida, tendría que volver a completarlo.

	Además, en consonancia con lo dicho en el apartado anterior de lecciones aprendidas, una modificación recomendable --pero significativamente costosa-- de cara a facilitar el trabajo futuro es la modificación del sistema existente sustituyendo la tecnología XML por JSON. Con esto, al ampliar el sistema con soporte para nuevos patrones se reduciría el esfuerzo de generar la estructura intermedia de unas 10 horas $\times$ persona (que es en lo que se estima con XML) a unas 5 horas $\times$ persona, a coste de una pequeña reducción de independencia entre los dos módulos.





