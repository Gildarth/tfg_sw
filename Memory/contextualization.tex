\chapter{Contextualización}
\minitoc
\label{chap:contextualizacion}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Objetivo: Contar cómo estaba la situación antes de empezar,                  %
%           todo lo que se hizo para familiarizarse con las tecnologías,       %
%           casarlas, etc.                                                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Este capítulo tiene por objetivo exponer aquellos conocimientos necesarios para entender el presente proyecto, desde el contexto histórico en el que se desarrolla hasta los conceptos, tecnologías y herramientas utilizados.



\section{Historia de los patrones y definición}
	
	En esta sección se pretende dar una visión histórica global sobre los patrones de diseño y su evolución, así como diversas definiciones actuales.
	
	Una de las primeras definiciones de lo que es un patrón de diseño fue documentada en el área de la arquitectura por Christopher Alexander en 1979 en su libro ``\textit{The Timeless Way of Building}''\cite{book:TimelessWayOfBuilding}, donde escribe: ``Cada patrón describe un problema que ocurre infinidad de veces en nuestro entorno, así como la solución al mismo, de tal modo que podemos utilizar esta solución un millón de veces más adelante sin tener que volver a pensarla otra vez''.
	
	Una de las primeras apariciones en la ingeniería del software la protagonizaron Ward Cunningham y Kent Beck en 1987 en su libro ``\emph{Using Pattern Languages for OO Programs}''\cite{book:UsingPatternLenguages}, donde usaron varias ideas de Alexander para desarrollar cinco patrones de interacción hombre-ordenador (HCI).
	
	Pero no sería hasta casi una década más tarde, en 1995, cuando Erich Gamma, Richard Helm, Ralph Johnson y John Vlissides, los llamados ``Gang of Four'' (GoF), publicaron su libro ``\emph{Design Patterns: Elements of Reusable Object-Oriented Software}'', donde ofrecieron una definición ampliamente aceptada aun a día de hoy:

        \begin{quotation}
          \itshape
	``Un patrón de diseño es una solución a un problema de diseño. Para que una solución sea considerada un patrón debe poseer ciertas características. Una de ellas es que debe haber comprobado su efectividad resolviendo problemas similares en ocasiones anteriores. Otra es que debe ser reutilizable, lo que significa que es aplicable a diferentes problemas de diseño en distintas circunstancias.''
        \end{quotation}
	
	También en palabras de GoF, los patrones de diseño pretenden:
	
	\begin{itemize}		 	
		\item Proporcionar catálogos de elementos reusables en el diseño de sistemas software.
		
		\item Evitar la reiteración en la búsqueda de soluciones a problemas ya conocidos y solucionados anteriormente.
		
		\item Formalizar un vocabulario común entre diseñadores,
		estandarizando el modo en que se realiza el diseño.
		
		\item Facilitar el aprendizaje de las nuevas generaciones de diseñadores condensando conocimiento ya existente.
	\end{itemize}
	
\section {Patrones de diseño software}
	
	Como hemos comentado, los patrones del diseño de software son aquéllos aplicados a la ingeniería del software y se catalogan en 3 grupos basándose en sus funciones~\cite{book:GoFBook}: creacionales (creación y destrucción de objetos), estructurales (relaciones entre clases e interfaces) y de comportamiento (delegación del comportamiento entre diferentes clases).

	Por ser los concernientes a este proyecto, nos centraremos en los patrones encargados del comportamiento.

	\subsection{Patrones de comportamiento}
	
		Los patrones de comportamiento son aquéllos que están relacionados con algoritmos y con la asignación de responsabilidades de soporte a la lógica de negocio a los objetos.
		
		Describen no solamente estructuras de relación entre objetos o de clases, sino también engloban esquemas de comunicación entre ellos y reparto de roles. Al igual que los otros tipos de patrones, se pueden clasificar en función de que trabajen con clases (\textit{Template Method, Interpreter}) u objetos (\textit{Chain of Responsability, Command, Iterator, Mediator, Memento, Observer, State, Strategy, Visitor}).

		
		A continuación se detallan un poco más aquellos patrones en concreto a los que se espera dar soporte en este proyecto, incluyéndose un análisis más detallado en el apartado correspondiente al análisis y diseño (capítulo~\ref{chap:design}).
		
		\subsubsection{Estado}
			
			El patrón de diseño Estado (\textit{State})\cite{book:GoFBook} se utiliza cuando el comportamiento de un objeto cambia dependiendo del estado del mismo. Surgió para solucionar el caso en que un contexto que se está desarrollando requiere tener diferentes comportamientos según el estado en que se encuentra, resultando complicado manejar el cambio de comportamientos dentro del mismo bloque de código. El patrón Estado propone una solución a esta complejidad, creando un objeto por cada estado posible donde encapsular la lógica relacionada.
				
			Se ofrece una interfaz estado genérica con un método para cada comportamiento que dependa del estado en que se encuentre el contexto. Este estado genérico se implementa para cada estado diferente en el que pueda estar el contexto y se da una implementación a cada acción variable en función del estado que se represente. El contexto entonces pasa a delegar la ejecución de estas acciones en el estado actual que contenga, siendo el propio estado el encargado de modificar el estado del contexto si procede (son los propios estados los encargados de modificar el estado interno del contexto). Con esto se consigue que el contexto pueda ejecutar sus funcionalidades y modificar el resultado de aquellas dependientes del estado durante la ejecución, sin necesidad de ser consciente de en qué estado se encuentra.

			El acoplamiento entre el contexto y la jerarquía de estados es, naturalmente, fuerte. En contraste, la jerarquía de estados no es visible a los objetos clientes del contexto, lo cual también contribuye a la mantenibilidad y extensibilidad de la solución que encarna este patrón.
			
			Un ejemplo sencillo de aplicación del patrón estado es un servicio TCP. Ante el mismo mensaje recibido, el comportamiento y respuestas devueltos dependerán del estado en el que se encuentre el servicio, y a su vez conducirán a la conexión a un estado concreto dependiendo del caso.
		
		\subsubsection{Estrategia}
		
			El patrón Estrategia (\textit{Strategy})\cite{book:GoFBook} permite mantener un conjunto de algoritmos de entre los cuales el objeto cliente puede elegir aquel que le conviene e intercambiarlo dinámicamente según sus necesidades.
			
			Cualquier programa que ofrezca un servicio o función determinada, que pueda ser realizada de varias maneras, es candidato a utilizar el patrón estrategia. Puede haber cualquier número de estrategias y cualquiera de ellas debe poder ser intercambiada por otra en cualquier momento, incluso en tiempo de ejecución, sin que el resultado final de ejecutarla varíe.
			
			Un ejemplo de esto son los algoritmos de ordenación, los cuales todos cumplen el mismo objetivo y dan el mismo resultado, pero de diferente manera.
			
			Esto se hace mediante una interfaz común que será implementada por cada estrategia concreta. Lo que diferencia este patrón de una simple especialización es el hecho de que cambiar la implementación concreta que se esté usando no conlleva cambios en aspectos funcionales de la aplicación, pero sí puede hacerlo en aspectos no funcionales como el rendimiento en función del caso de trabajo concreto.
			
			Aunque la estructura de clases propuestas es similar al del patrón Estado, el acoplamiento entre el objeto cliente y las estrategias en este caso es mínimo; de hecho, las estrategias son independientes de sus clientes, y tienen vocación de ser reutilizables, por lo que no restringen su visibilidad dentro del sistema.
		
		\subsubsection{Observador}
		
			El patrón de comportamiento Observador (\textit{Observer})\cite{book:GoFBook} define una dependencia del tipo uno-a-muchos entre objetos, de manera que cuando el uno cambia su estado, notifica este cambio a todos los dependientes.
			
			Este patrón también se conoce como el patrón de suscripción-publicación. Este nombre sugiere la idea básica del patrón, que es: el objeto de interés (observado), se responsabiliza de gestionar y notificar al conjunto de objetos interesados (observadores) que se pueden suscribir a él. Los observadores tienen una dependencia con el observado, ya que tras ser notificados pueden enviar peticiones al mismo, según la lógica que implementen. Eso sí, los observadores son independientes entre sí, no tienen conocimiento mutuo, y por tanto el orden de notificación de los observadores y el orden de actuación de los observadores sobre el observado debe implementarse de manera coherente en cada entorno de aplicación.
			
			Este patrón suele aplicarse en los \textit{frameworks} de interfaces gráficas orientados a objetos, en los que la forma de capturar los eventos es suscribir \textit{listeners} a los objetos que pueden disparar eventos. También es la clave del patrón de arquitectura Modelo Vista Controlador (MVC)\cite{book:ArquitecturesSw}.
			
	\section{Pruebas}
		
		Las pruebas software \cite{book:Pruebas} son el principal medio que se tiene para buscar errores en un sistema o aplicación en construcción. Existen diferentes tipos de pruebas en función de en qué punto del sistema se busquen los errores, la estrategia utilizada para localizarlos, o el tipo de errores que se persiguen, pero todas tienen en común que se consideran exitosas cuando encuentran un error (ya que es entonces cuando cumplen su cometido y demuestran su valor).
		
		Por norma general, el tamaño del software actual hace imposible probar un programa o sistema por completo, e incluso el mejor conjunto de pruebas no puede garantizar la ausencia de fallos sin revelar.
		Dicho de otro modo,
		tanto por la imposibilidad de probarlo todo, como por el hecho de que las pruebas en sí mismas son código fuente (y por tanto, sujetas a errores en sí mismas), éstas nunca podrán demostrar que un software está libre de fallos. Lo único que pueden demostrar es o bien que el software probado tiene algún fallo, o que no se ha sido capaz de descubrir ningún fallo con esas pruebas.	
		
		Una buena práctica a la hora de desarrollar software es el tener al menos una prueba por cada unidad de implementación, funcionalidad y requisito, llegando al punto de que algunas metodologías ágiles modernas sugieren escribir las pruebas antes incluso que el programa.
		
		Habitualmente se clasifican las pruebas en cuatro niveles: 
		
		\begin{description}
			\item[Pruebas unitarias] utilizadas para secciones específicas y delimitadas, sin dependencias de otras unidades de implementación (servicios, otros módulos, etc\dots{}). 
			\item[Pruebas de integración] cuya finalidad es probar la correcta interrelación de componentes. 
			\item[Pruebas de sistema] realizadas por los desarrolladores sobre el sistema software como un todo.
			\item[Pruebas de aceptación] llevadas a cabo por el usuario final sobre el software completo.
		\end{description}
		
		Otras clasificaciones complementarias habituales son, según el conocimiento que se tiene del software a probar:
		
		\begin{description}
			\item[Pruebas de caja blanca] cuando se realizan utilizando conocimiento de cómo es internamente.
			\item[Pruebas de caja negra] cuando no se conoce o se tiene acceso a la estructura interna (implementación) de lo probado y solo se trabaja en base a su API externa (entradas y salidas).
		\end{description}
		
		Según el tipo de requisito a probar, también se puede hablar de:
		
		\begin{description}
			\item[Pruebas funcionales] para probar que el software cumple con la funcionalidad de negocio esperada.
			\item[Pruebas no funcionales] cuando lo que se prueba es cómo el software lleva a cabo sus funcionalidades de negocio en términos de calidad (por ejemplo, consumo de recursos).
		\end{description}

		Según si ejecuta o no el código fuente:
		
		\begin{description}
			\item[Pruebas estática] cuando analizan código pero no lo ejecutan.
			\item[Pruebas dinámicas] en aquellos casos que la ejecución de la prueba requiera ejecutar el código probado.
		\end{description}

		Desarrollar un buen conjunto de pruebas automatizadas es de gran ayuda, sobretodo a largo plazo, pudiendo ejecutarlas siempre que se haga alguna modificación o ampliación en el software (denominándose en esta utilidad, ``pruebas de regresión''), lo que ayuda a detectar errores de forma sencilla. Esto en proyectos grandes donde cualquier modificación pueda desencadenar (con mayor probabilidad cuanto peor sea el diseño) un efecto mariposa en la parte menos esperada, es de gran utilidad, pues permite probar los fallos que puede haber.

	\subsection{Pruebas unitarias}

		Como hemos dicho, las pruebas unitarias son aquellas cuya finalidad es probar, de forma atómica e independiente del resto del sistema, un fragmento de código o módulo. Para ello suelen usarse herramientas que permiten escribir comprobaciones centradas en cada una de las funcionalidades específicas, sin dependencias con otras. Un ejemplo de prueba unitaria sería la prueba de que la inserción de datos en una estructura resulta en la modificación de ésta, de manera que una consulta sobre la misma revela que efectivamente los datos se han persistido.

	\subsection{Herramientas XUnit}

		\label{sec:XUnit}

		Existen varios frameworks de ayuda al desarrollo de pruebas y comprobaciones a nivel de unidad, que han llegado a conocerse colectivamente como xUnit\cite{website:XUnit}. Tales frameworks están basados en un diseño de Kent Beck, implementados originalmente para Smalltalk como SUnit, pero están ahora disponibles para muchos lenguajes de programación y plataformas de desarrollo.

		El diseño general de los frameworks xUnit\cite{website:XUnit} depende de varios componentes:

		\begin{description}
	
			\item[Caso de prueba (\textit{test case})] es el componente elemental del framework. Todas las pruebas definidas por el desarrollador serán de este mismo tipo.
	
			\item[Motor de pruebas (\textit{test runner})] es un programa ejecutable que corre las pruebas implementadas usando el framework XUnit y genera un informe con los resultados de dicha ejecución.
	
			\item[Conjunto de pruebas (\textit{test suite})] está formado por el conjunto de casos de prueba agrupados en uno o varios módulos de pruebas. El orden de su ejecución no debe ser relevante.
	
			\item[Accesorios de prueba (\textit{test fixture})] es un conjunto de precondiciones o estados necesarios para que se ejecute un caso de prueba. También se conoce a esto como contexto de prueba.
	
			\item[Ejecución de prueba (\textit{test execution})] es la ejecución de un caso de prueba unitaria individual. Primero se prepara el contexto donde se ejecutarán los casos, seguido de la ejecución en orden no determinista de los mismos y para finalizar se limpia el contexto retornándolo a su estado anterior para que la ejecución de unas pruebas no influya en otras o en la ejecución normal del programa.
	
			\item[Formateador de resultados (\textit{result formatter})] se encarga de generar el informe de ejecución en diferentes formatos, entre ellos habitualmente HTML o XML.
	
			\item[Aserciones (\textit{assertions})] son funciones o macros que verifican el comportamiento (o estado) de aquello que está siendo probado. Generalmente son condiciones lógicas que comparan un resultado esperado con el obtenido tras la ejecución del código a probar.
		\end{description}

		Una prueba puede terminar con uno de estos tres resultados: que sea exitosa (todas las aserciones se han evaluado como verdaderas), que haya un error (una aserción se ha determinado falsa) o que se produzca un fallo (se ha producido algún error inesperado durante al ejecución).

		\subsection{JUnit}
		\label{sec:context_JUnit}

		JUnit\cite{website:JUnit} es un framework de pruebas de la familia XUnit para el lenguaje Java. Originalmente fue escrito por Erich Gamma y Kent Beck y actualmente es un proyecto de código abierto bajo la licencia pública de Eclipse en su versión 1.0, estando almacenado su código en SourceForge\cite{website:SourceForge}.

		Un accesorio de prueba JUnit es un objeto Java y sus métodos de prueba han de ir precedidos de la anotación \verb+@Test+. Contiene anotaciones \verb+@BeforeClass+ y \verb+@AfterClass+ para especificar código que se debe ejecutar al iniciar la ejecución de la suite de pruebas y al terminarla (respectivamente). De manera similar, las anotaciones \verb+@Before+ y \verb+@After+ indican código que ha de ejecutarse antes y después, respectivamente, de cada prueba unitaria. También proporciona un conjunto de aserciones, siendo algunas de ellas \verb+AssertEquals, AssertTrue y AssertFalse+. El orden de ejecución de las pruebas, como es habitual, no está asegurado.


	\subsection{XMLUnit}



		XMLUnit\cite{website:XMLUnit} es un framework de la familia XUnit\cite{website:XUnit} para Java\cite{website:Java} y .Net que añade aserciones para validar y comparar documentos XML, diferenciando estas comparaciones entre documentos idénticos (los mismos elementos formando la misma estructura arbórea en el mismo orden), similares (los mismos elementos formando la misma estructura arbórea en distinto orden) o distintos (algún elemento es hijo de un elemento diferente al esperado).

			


\section{Ciclo de vida}

	El ciclo de vida \cite{book:IngenieriaSW} es el conjunto de fases por las que pasa el sistema que se está desarrollando desde que nace la idea inicial hasta que el software es retirado o remplazado (muere).
		
	Entre las funciones que debe tener un ciclo de vida se pueden destacar:
		
	\begin{itemize}
		\item Determinar el orden de las fases del proceso de software
		
		\item Establecer los criterios de transición para pasar de una fase a la siguiente
		
		\item Definir las entradas y salidas de cada fase
		
		\item Describir los estados por los que pasa el producto
		
		\item Describir las actividades a realizar para transformar el producto
		
		\item Definir un esquema que sirve como base para planificar, organizar, coordinar y desarrollar. 
	\end{itemize}
	
	El proceso para el desarrollo de software, también denominado ciclo de vida del desarrollo de software, es una estructura aplicada al desarrollo de un producto de software. Hay varios modelos a seguir para el establecimiento de un proceso para el desarrollo de software, cada uno de los cuales describe un enfoque diferente para diferentes actividades que tienen lugar durante el proceso.
	
	\subsection{Ciclo de vida en espiral}
		
		\label{sect:spiral_life_cicle}
	
		El desarrollo en espiral es un modelo de ciclo de vida del software definido por primera vez por Barry Boehm en 1986 en su artículo ``{\itshape A Spiral Model of Software Development and Enhancement}'', y se utiliza con frecuencia en la ingeniería de software. Las actividades de este modelo se conforman en una espiral, en la que cada bucle o iteración representa un conjunto de actividades. Las actividades no están fijadas a ninguna prioridad, sino que las siguientes se eligen en función del análisis de riesgo, comenzando por el bucle interior.
	
	
		En cada vuelta o iteración hay que tener en cuenta:
		
		\begin{description}
			
			\item [Los Objetivos:] 
				qué necesidad debe cubrir el producto.
			
			\item [Alternativas:] 
				las diferentes formas de conseguir los objetivos de forma exitosa, desde diferentes puntos de vista como pueden ser:
				
				\begin{enumerate}
					\item 
						Características: experiencia del personal, requisitos a cumplir, etc.
					
					\item 
						Formas de gestión del sistema.
					
					\item 
						Riesgo asumido con cada alternativa.
				\end{enumerate}
					
			\item [Desarrollar y Verificar:] 
				programar y probar el software.
			
		\end{description}
		
		Si al final de un ciclo el resultado no es el adecuado o se necesita implementar mejoras o funcionalidades, se planificaran los siguientes pasos y se comienza un nuevo ciclo de la espiral. La espiral tiene una forma de caracola y se dice que mantiene dos dimensiones, la radial (indica el avance del proyecto del software dentro de un ciclo) y la angular (indica la cantidad de iteraciones y por ende el aumento en tiempo y coste del proyecto).
		
		Como se ve en la figura \ref{pic:context_spiral_cicle}, para cada ciclo habrá cuatro fases: 
		
		\begin{enumerate}
			\item
				\textit{Determinar Objetivos:} Se recogen y especifican los requisitos. Se identifican los riesgos del proyecto y se plantean estrategias alternativas para evitarlos. Un caso especial de esta fase es la planificación inicial, que solo se realiza en al primera iteración.
				
			\item
				\textit{Análisis del riesgo:} Se lleva a cabo el estudio de las causas de las posibles amenazas y probables eventos no deseados, así como los daños y consecuencias que éstas puedan producir. Se evalúan alternativas.
			
			\item
				\textit{Desarrollar y probar:} Dependiendo del resultado de la evaluación de los riesgos, se elige un modelo adecuado para el desarrollo de entre los existentes. Así si por ejemplo los riesgos en la interfaz de usuario son dominantes, un modelo de desarrollo apropiado podría ser la construcción de prototipos evolutivos.
			
			\item
				\textit{Planificación:} Se presenta el estado y la evolución del proyecto y durante la iteración. A partir de ello se deciden los objetivos a cumplir en la siguiente iteración, que pueden ser desde arreglar errores surgidos de la anterior iteración a incluir nuevas funcionalidades.
				
		\end{enumerate}		

		
		\begin{figure}
			\includegraphics[width=30em]{ModeloEspiral.png}
			\centering
			\caption{Esquema del modelo en espiral}
			\label{pic:context_spiral_cicle}
		\end{figure}
		
		


\section{Java}

	Java\cite{website:Java} es un lenguaje de programación orientado a objetos de propósito general, que fue diseñado específicamente para tener tan pocas dependencias de implementación como fuera posible. Su intención es permitir que los desarrolladores de aplicaciones escriban el programa una vez y lo ejecuten en cualquier dispositivo (conocido en inglés como WORA, o ``\textit{write once, run anywhere}''), lo que quiere decir que el código que es ejecutado en una plataforma no tiene que ser recompilado para correr en otra, ya que la ejecución se realiza a través de una máquina virtual. Java es, a partir de 2012, uno de los lenguajes de programación más populares en uso, particularmente para aplicaciones de cliente-servidor de web, con 9 millones de usuarios-desarrolladores en todo el mundo \cite{website:JavaStatistics}.
	
	El lenguaje de programación Java fue originalmente desarrollado por James Gosling de Sun Microsystems (la cual fue adquirida por la compañía Oracle) y publicado en 1995 como un componente fundamental de la plataforma Java de Sun Microsystems. Su sintaxis deriva en gran medida de C y C++, pero tiene menos utilidades de bajo nivel que cualquiera de ellos. Las aplicaciones de Java son generalmente compiladas a \textit{bytecode} (código objeto) que puede ejecutarse en cualquier máquina virtual Java (JVM) escrita en código nativo de la máquina en que se ejecuta, por lo que el programa se ejecuta sin importar la arquitectura de la computadora subyacente. Además, se suministran bibliotecas adicionales para acceder a las características de cada dispositivo (como los gráficos, ejecución mediante hilos o \textit{threads}, la interfaz de red) de forma unificada. Se debe tener presente que, aunque hay una etapa explícita de compilación, el \textit{bytecode} generado es interpretado o convertido a instrucciones máquina del código nativo por el compilador JIT (\textit{Just In Time}).
	
	La compañía Sun desarrolló la implementación de referencia original para los compiladores de Java, máquinas virtuales, y librerías de clases en 1991 y las publicó por primera vez en 1995. A partir de mayo de 2007, en cumplimiento con las especificaciones del Proceso de la Comunidad Java, Sun volvió a licenciar la mayoría de sus tecnologías de Java bajo la Licencia Pública General de GNU. Otros también han desarrollado implementaciones alternas a estas tecnologías de Sun, tales como el Compilador de Java de GNU \cite{website:GCJ} y el GNU Classpath\cite{website:GNUClasspath}.
	
	En Java el problema de fugas de memoria se evita en gran medida gracias a la recolección de basura automática (o ``\textit{automatic garbage collection}''). El programador determina cuándo se crean los objetos y el entorno en tiempo de ejecución de Java (Java RTS) es el responsable de gestionar el ciclo de vida de los objetos. El programa, u otros objetos pueden tener localizado un objeto mediante una referencia a éste. Cuando no quedan referencias a un objeto, el recolector de basura de Java borra el objeto, liberando así la memoria que ocupaba previniendo posibles fugas. Aun así, es posible que se produzcan fugas de memoria si el código almacena referencias a objetos que ya no son necesarios, pero ya no es el programador quien ha de hacerlo explícitamente como ocurre en C o C++.
	
	Estas facilidades al desarrollo suponen que en lo referente al rendimiento no alcanze las cotas de los ya mencionados C o C++.

\section{Apache Maven}

	Apache Maven \cite{book:Maven} es un software para la gestión de proyectos escritos en Java. Permite compilación, ejecución, ejecución de pruebas, generación de informes, documentación técnica\dots{} de una forma centralizada. Facilita la gestión de múltiples proyectos relacionados como un todo en lugar de tener que gestionarlos uno a uno por separado.
	
	Fue creado en 2002 por Jason van Zyl, de Sonatype, y actualmente es desarrollado por la ``Apache Software Foundation''\cite{website:ApacheFoundation}.


		

\section{Marcadores en código}

	A la hora de señalar fragmentos de código para algún framework, es común el uso de marcadores en el mismo código. Estos marcadores pueden ser ignorados por el compilador (por estar escritos en comentarios por ejemplo) e interpretados por otro programa que analice el código fuente, como es el caso de la herramienta Javadoc\cite{website:Javadoc}, o pueden desencadenar la ejecución de otro código enlazado a la anotación como es el caso de Spring\cite{website:Spring} para tareas como la inyección de dependencias.


\section{XML}

	Lenguaje de Marcado eXtensible (``EXtensible Markup Language'') o XML es un lenguaje de marcado desarrollado por el World Wide Web Consortium (W3C) utilizado para almacenar datos en forma legible para las personas. Deriva del lenguaje SGML (Standard Generalized Markup Language) \cite{website:ISOSGML} y permite estructurar documentos grandes mediante la definición de una gramática específica.

	XML se propone como un estándar para el intercambio de información estructurada entre diferentes plataformas, permitiendo compartir la información de una manera segura, fiable y fácil.
	
	La estructura fundamental de un documento XML se basa en un árbol de trozos de información. Estos trozos se llaman elementos y se los señala mediante etiquetas, que consisten en marcas hechas en el documento señalando una porción de éste como un elemento, el cual puede contener más elementos, caracteres o ambos, o bien ser un elemento vacío. Los elementos también pueden tener atributos, que son una manera de incorporar características o propiedades a los elementos de un documento. Las etiquetas tienen la forma \verb+<nombre atributo="valor" />+ o \verb+<nombre atributo="valor"> elemento </nombre>+, donde \verb+nombre+ es el nombre del elemento que se está señalando.
	
	El prólogo de un documento XML contiene una declaración XML, una declaración de tipo de documento y uno o más comentarios e instrucciones de procesamiento. Ej: \verb+<?xml version="1.0" encoding="UTF-8"?>+.
	
	También se permite el uso de comentarios a modo informativo para el programador que han de ser ignorados por el procesador. Los comentarios en XML tienen el siguiente formato:  \verb+<!--- comentario --->+.
	
	Un documento se considera ``bien formado'' si su estructura sintáctica básica está correctamente escrita según el formato XML. Ahora bien, cada aplicación de XML (es decir, cada lenguaje definido con esta tecnología), necesitará especificar cuál es exactamente la relación que debe verificarse entre los distintos elementos presentes en el documento, lo cual se define en un documento externo (expresada como DTD —Document Type Definition, 'Definición de Tipo de Documento'— o como XSchema). Crear una definición equivale a crear un nuevo lenguaje de marcado, para una aplicación específica.
	
	Entre sus ventajas encontramos que se trata de un lenguaje extensible en el que se pueden seguir añadiendo etiquetas a una vez ya ha sido puesto en producción, el analizador es un componente estándar y por lo tanto no es necesario crear uno específico para cada lenguaje XML definido y tiene una buena compatibilidad entre aplicaciones al ser fácil de entender su estructura y contenido por parte de un tercero.
	
	De sus inconvenientes destaca el hecho de que, al tener una estructura arbórea inherente, puede necesitar mucha memoria para tenerlo cargado en memoria y leerlo.


\section{Generación automática de código}


	En la actualidad son cada vez más comunes las herramientas que generan código de manera automática, ayudando al desarrollador desde auto-completando el nombre de una variable hasta escribiendo fragmentos completos de código, como pueda ser una herramienta de generación de código a partir de un diagrama UML.

	Dichas herramientas suponen una gran ayuda, pues no solo ahorran horas de trabajo (generalmente en tareas repetitivas y monótonas como escribir en una clase sus atributos y el conjunto genérico de métodos para acceder a ellos), sino que además se reducen las posibilidades de introducir errores en dichos fragmentos de código, en comparación con la implementación manual.

\section{Sistema de nombrado}
	\label{sec:nomenclatura}
	
	A lo largo del proyecto se han adoptado diversas convenciones a la hora de nombrar los elementos que lo componen. En esta sección se recogen dichas convenciones.
	
	\begin{description}
		
		\item [Objetivos] \textbf{Objetivo-XX}
			Donde XX es un código numérico correlativo.
		
		\item [Requisitos] \textbf{R[M$\mid$A][F$\mid$NF]-XX: }
			Donde R es de requisito, M de mínimo, A de adicional, F de funcional, NF de no funcional y XX es un código numérico correlativo de dos dígitos para diferenciar los requisitos que sean de la misma categoría y mismo tipo. Ejemplo ``RMF-01'' para el requisito mínimo funcional número 1.
		
		\item [Pruebas] \textbf{P[F$\mid$NF][E$\mid$EST]-X: }
			Donde P es de prueba, F de funcional, NF de no funcional, E de estado, EST de estrategia y X es un código número correlativo dentro de la misma categoría para el mismo patrón. Ejemplo ``PNFEST-1'' para la prueba no funcional del patrón estado número 1.
			
		\item [Riesgos] \textbf{R[E$\mid$EST]-XX: }
			Donde R es de Riesgo, E de estado, EST de estrategia y XX es un código numérico correlativo de dos dígitos que diferencia los riesgos dentro de los pertenecientes al mismo patrón. Ejemplo ``RE-02'' para el riego numero 2 identificado para la iteración del patrón estado.
			
		\item [Tests del sistema] \textbf{itX\_ftYY\_ZZ\_test: }
			Donde X es el número de la iteración donde se ha implementado, ft es de ``\emph{functional test}'', YY es un código numérico correlativo dentro de la iteración y ZZ es un nombre adecuado a aquello que se esté probando. Además, \textbf{itX\_ftYY} supone una clave única, y por ello en este documento se identificarán las pruebas solo por ella, sin necesidad de añadir el resto del nombre, pues éste puede llegar a ser bastante largo. Ejemplo ``it1\_ft12\_test\_name\_example\_test'' para la prueba número 12 de la primera iteración.
		
	\end{description}
	
	En el código del proyecto, se ha utilizado el inglés para dar nombre a las clases, métodos y atributos, así como para escribir los comentarios, diseñar los marcadores y la representación intermedia. Con esto se pretende aumentar el número de desarrolladores potenciales.
	
	Las diferentes implementaciones de interfaces comunes a cada patrón se han nombrado mediante el nombre del patrón seguido de ``\texttt{Pattern}'' y terminado en el nombre de la interfaz en sí. Por lo tanto, la clase que implemente una interfaz ``\texttt{Parser}'' para el patrón Estado se llama ``\texttt{StatePatternParser}''.
	
	A las pruebas resultantes de la ejecución del sistema se las ha nombrado siguiendo la convención ``testXXX\_YYY ()'' siendo XXX un código numérico correlativo e YYY un nombre adecuado a la prueba concreta con ``\_'' en lugar de espacios.


