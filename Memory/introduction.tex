\chapter{Introducción}
\minitoc
\label{chap:introduccion}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Objetivo: Exponer de qué va este proyecto, sus líneas maestras, objetivos,   %
%           etc.                                                               %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

En este capítulo se describen los rasgos generales del proyecto en su conjunto.


\section{Motivación}
	
	 En la actualidad los patrones de diseño de la ingeniería del software se han convertido en un conocimiento obligatorio para cualquiera que se dedique a la profesión, pues permiten resolver de forma elegante y efectiva situaciones muy habituales. 
	 
	 No obstante, pese a su extendido uso, no existen herramientas ni librerías de pruebas genéricas que puedan aplicarse sobre el patrón en sí, lo que en la práctica lleva a realizarlas \textit{ad-hoc} para cada implementación mezclándose con las pruebas del problema concreto al que se ha aplicado el patrón. 
	 
	 Por ejemplo, la función del patrón Observador es que, al modificarse el componente observado, todo componente observador sea avisado y analice los cambios en el observado, realizando la lógica que le corresponda en caso de considerarlo necesario. Qué particularidades del cambio en el observado analice cada observador y qué lógica correspondiente debe realizar es particular de cada uso del patrón y no interfiere con la funcionalidad propia del patrón. Dicho de otro modo, sea cual sea la lógica de negocio de observado y observadores, cualquier implementación de este patrón debe ir acompañada de las pruebas correspondientes relacionadas con la adecuada creación y recepción de notificaciones.
	 

\section{Objetivos}

	Con la motivación presentada en la sección anterior como base, el presente trabajo pretende analizar algunos patrones de los descritos en el libro de GoF \cite{book:GoFBook}, catalogados dentro de la categoría ``de comportamiento'', con la intención de definir un conjunto de pruebas genéricas, independientes de la implementación concreta, que permitan probar cualquier implementación del patrón de forma automatizada y lo más completa posible, con la mínima interacción necesaria por parte del usuario. En concreto, se pretende realizar el soporte del sistema para los patrones Estado, Estrategia y Observador.
	
	Con este producto, se espera motivar e incentivar el uso consciente e intencionado de los patrones por parte de los desarrolladores de software, al permitirles probar una parte importante de su software (ya que los patrones elegidos son los que más intervienen en la lógica de la aplicación) con un mínimo de esfuerzo y la fiabilidad de que son un conjunto completo y estable de pruebas con capacidad de detectar los fallos que buscan, en caso de existir. 
	
	Como consecuencia última se cree que esto contribuirá a mejorar la calidad del software en general, pues si bien las pruebas no garantizan la calidad del producto, sí que ayudan a verificar que el software alcanza unos mínimos validables, y consituye un elemento de mantenibilidad muy valioso. Además, el incentivar el uso de patrones también ayudará a mejorar la calidad del software, pues éstos permiten solucionar problemáticas del desarrollo de forma eficaz, escalable y, gracias a herramientas como esta, fácilmente validable.
	
	Para que el proyecto pueda ser utilizable, se va a poner a disposición de quien lo quiera mediante un repositorio git \cite{website:GIT} público alojado en la URL: 
	
	\begin{verbatim}
	https://bitbucket.org/Gildarth/tfg_sw.git
	\end{verbatim}
	
	Un punto importante del trabajo es la escalabilidad del producto, de forma que añadir más lenguajes o tecnologías de pruebas automatizadas (como es el caso de JUnit para Java por ejemplo) sea lo más sencillo posible. Para ello, se separará en dos módulos comunicados por una representación intermedia abstracta e independiente. El primer módulo será el encargado de generar la representación intermedia del patrón, siendo el segundo el encargado de entenderla y generar los ficheros de prueba correspondientes. Gracias a la representación intermedia, cada módulo es independiente del otro. No obstante, se considera buena idea estandarizar una interfaz externa para cada uno, de forma que pueda ser utilizado por otros sistemas a modo de componente (por ejemplo, que pueda ser integrado dentro de un entorno de desarrollo como Eclipse \cite{website:Eclipse}).
	
	En resumen, y por orden de relevancia, los objetivos concretos establecidos son:
	
	\begin{description}
		
		\item [Objetivo-1] Que el sistema sea escalable, de forma que añadir el soporte a nuevos patrones suponga el menor esfuerzo posible.
		
		\item [Objetivo-2] Analizar el patrón Estado y diseñar un conjunto de pruebas genéricas, independientes y de generación lo más automatizada posible.
		
		\item [Objetivo-3] Implementar el soporte del sistema para el patrón Estado.
		
		\item [Objetivo-4] Analizar el patrón Estrategia y diseñar un conjunto de pruebas genéricas, independientes y de generación lo más automatizada posible.
		
		\item [Objetivo-5] Implementar el soporte del sistema para el patrón Estrategia.
		
		\item [Objetivo-6] Analizar el patrón Observador y diseñar un conjunto de pruebas genéricas, independientes y de generación lo más automatizada posible.
		
		\item [Objetivo-7] Implementar el soporte del sistema para el patrón Observador.
		
	\end{description}
		
	El esfuerzo total estimado para este tipo de proyecto debe rondar las 300 horas$\times$persona, a razón de tener una valoración de 12 créditos ECTS y el esfuerzo estimado para cada crédito ECTS ser de 25 horas$\times$persona.	

	La fecha de inicio es el 17 de julio de 2015 y la de finalización el 10 de febrero de 2016 (fecha de depósito de la memoria). 
	
\section{Metodología}

	Debido a la estabilidad del contexto del trabajo y la forma del software planteado, se ha decidido seguir un ciclo de vida en espiral\cite{book:IngenieriaSW}, en el cual a cada iteración se añadirá un nuevo patrón al sistema.

	Durante todo el desarrollo se van a seguir las convenciones de nombrado que se describen en la sección  \ref{sec:nomenclatura}.
	
\section{Estructura de este documento}

	La presente memoria se estructura en: introducción (la presente parte), contextualización, descripción de requisitos, detalle de planificación, análisis y diseño, implementación y validación, seguimiento, y conclusiones.
	
	En el capítulo de contextualización se describirán todos aquellos componentes y conocimientos necesarios para entender el presente trabajo. Aquí se incluye la sección de nomenclatura, donde se explican las diferentes convenciones de nombrado adoptadas en el proyecto.
	
	En el capítulo de descripción de requisitos se detallan aquellos requisitos que componen el proyecto y se catalogan según si son o no funcionales y según su relevancia en el producto final. La valoración de esfuerzo para cada requisito se deja para la planificación, pues el mismo requisito común en las distintas iteraciones pueden requerir un esfuerzo diferente.
	
	Las iteraciones se presentan repartidas en cuatro capítulos: Planificación, Análisis y diseño, Implementación y Validación, y Seguimiento; en ellos se detalla el desarrollo del proyecto siguiendo las fases del ciclo de vida en espiral.
	
	En las conclusiones se presenta un análisis de los problemas surgidos durante el desarrollo del trabajo y las lecciones aprendidas de dichos problemas, seguido del trabajo que ha quedado más allá del alcance de este trabajo, así como las vías de desarrollo futuro que se ven para el sistema.
	
	
	
	
	
	
	
	
